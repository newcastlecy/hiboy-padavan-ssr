#hiboy Padavan 固件 SSR MOD
 :point_right: hiboy Padavan 3.4.3.9-099_07-06固件已自带SSR 如不出意外 本项目不再更新  
 :point_right: SS广告：[ManSoRa](https://ss.ime.moe/) :rocket: 
### 修改脚本下载SSR主程序   
- **注意：MT7621A SOC的路由最好使用breed超频至1.2GHz(注意散热) 以获得更好的运算能力**  
- "扩展功能设置" - "脚本功能设置" - "强制安装opt？"请务必打开，"启用 opt 自动更新功能"请最好打开，方便SSR MOD升级，"opt安装方案选择" 如果有SD卡请选择"安装到SD卡"，如果使用U盘请选择"安装到U盘" ，都没有请使用"安装到内存"（使用内存作opt最好打开Swap）
- "自定义设置" - "脚本" - "脚本0" 找到 "# opt安装"   
**注意：每次更新固件后都需要重新修改脚本0**   
ssfile='http://git.oschina.net/love4taylor/hiboy-padavan-ssr/raw/master/optupang6.tgz'  
ssfile2='http://git.oschina.net/love4taylor/hiboy-padavan-ssr/raw/master/opttmpg6.tgz'  
ssfile3='http://git.oschina.net/love4taylor/hiboy-padavan-ssr/raw/master/optg6.txt'  

![修改脚本](http://git.oschina.net/uploads/images/2016/0611/152617_10c5fc83_58578.png "修改脚本")
- 最后写入闪存以免重启复原，点击后CPU会稍微升高，等几秒到十几秒就好了  
![写入闪存](http://git.oschina.net/uploads/images/2016/0530/190306_71860972_58578.png "写入闪存")
- 重启路由让脚本生效(重启前请确认是否已经写入闪存   

### SSR使用方法
- 在高级启动参数选项里填入SSR的混淆以及协议  
格式 -o 混淆 -O 协议 -g 混淆自定义参数  
例子：-o tls1.2_ticket_auth -O auth_sha1_v2 -g cloudfront.net  
注意：目前仅 http_simple，http_post，tls1.2_ticket_auth 这几个混淆方式支持自定义参数。  
注意：错误设置混淆自定义参数可能导致连接被断开甚至IP被封锁，如不清楚如何设置那么请不要使用-g，推荐设置为cloudflare.com或cloudfront.net  
具体混淆/协议的类型请询问SSR服务商  
SSR所有支持的混淆/协议类型 [点这里查看](https://github.com/breakwa11/shadowsocks-rss/blob/master/ssr.md)  
![SSR参数](http://git.oschina.net/uploads/images/2016/0530/193245_fdb0a31a_58578.png "SSR参数")  
  
### Breed 超频
![breed1](http://git.oschina.net/uploads/images/2016/0531/001602_ab88deb3_58578.png "breed1")
![breed2](http://git.oschina.net/uploads/images/2016/0531/001627_925b9546_58578.png "breed2")
